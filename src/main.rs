#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib; 
extern crate serde;
#[macro_use] extern crate serde_derive; // TODO -- WTF does macro_use mean?

mod post;
mod get;

use rocket_contrib::Template;

fn main() {
    rocket::ignite()
        .mount("/", 
            routes![
                get::get_html, 
                get::get_js,
                post::receive_message
            ])
        .attach(Template::fairing())
        .launch();
}
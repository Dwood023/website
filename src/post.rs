
use std::collections::HashMap;
use rocket_contrib::{Json, Template};

#[post("/chat", data="<message>")]
fn receive_message(message: Json<Message>) -> Template {
    let msg : Message = message.into_inner();

    let mut context : HashMap<String, String> = HashMap::new();
    context.insert("message".to_string(), msg.body);
    Template::render("message", &context)
}

#[derive(Deserialize)]
struct Message {
    body: String
}

use std::fs::File;
use rocket::http::RawStr;
use rocket::http::Cookies;

#[get("/<page>")]
fn get_html(page: &RawStr, cookies: Cookies) -> File {

    let url = format!("html/{}.html", page);

    if let Some(user) = cookies.get("user") {
        println!("{} visited by {}", page, user.value());
    }

    File::open(url).ok().unwrap() // TODO -- 404 resource not found
}

#[get("/js/<script>")]
fn get_js(script: &RawStr) -> File {
    let url = format!("js/{}", script);
    File::open(url).ok().unwrap() // TODO -- 404 resource not found
}

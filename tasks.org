* DONE Post without reloading page

* TODO Error handling
** TODO If resource doesn't exist -> 404

* DONE Make chat log
** DONE Div containing list of messages
** DONE Messages added by POST from message field
*** DONE Response handled
*** DONE Message wrapped in HTML, added to DOM
InsertAdjacentHTML() used with pre-templated HTML from server.
To keep client light, it looks good doing all the work in the backend. All the client is doing currently is async HTTP and inserting pre-processed HTML blocks in their entirety.
**** DONE Template for message? Add message to template, send in response
Response is a type of promise, with a member function then(fn_ptr) 
Calls fn_ptr when promise is fulfillied. Basically, for everything thats not immediately available, you'll need a callback. Can just be anonymous function taking promise as argument.

* TODO Messages are distinct between client senders
** TODO Client is linked to User
User authenticated, cookie recieved, cookie verified by rocket, User available to message eg. messages labelled by sender
*** TODO User authentication, private chat


* Features
- Private chat between two users
- Users are matched by some logic
- Personal data becomes available under conditions
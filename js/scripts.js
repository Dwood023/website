function post_field(field_id) {
    var field = document.getElementById(field_id);
    var field_value = field.value;

    var data = {body: field_value}

    post_json("/chat", data)
        .then(response => response.text())
        .then(function (text) {
            var chat_div = document.getElementById("chat_div");
            chat_div.insertAdjacentHTML("beforeend", text);
        })
}

function post_json(url, json_object) {
    return fetch(url, { 
        method: "post",
        headers: new Headers({"Content-Type" : "application/json"}),
        body: JSON.stringify(json_object)
    }) 
}
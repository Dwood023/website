* POST request body
** ![post("/"), data = "<req_body>"]
Post body is accessed by assigning it to a variable name under "data" in the handlers attributes.
** fn handler(req_body : A)
This variable is included in the function signature of the handler and must be of a type implementing trait FromData.
** FromData
FromData is implemented for basic types. Eg. String just puts the body in a String. Apparently doing this is a bad idea, there's no validation or bounds on the size of the request. Implementing for your own types makes exploits less likely.
** FromData User Types
You can implement FromData for your own Structs. This allows arbitrary validation before turning the data into a new struct instance. 
